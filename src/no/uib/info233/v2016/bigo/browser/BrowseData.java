package no.uib.info233.v2016.bigo.browser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import no.uib.info233.v2016.bigo.browser.trial.implementations.ArrayTrial;

public class BrowseData {
	
	//	0	1			2		3		4		5
	// id,first_name,last_name,email,gender,ip_address1
	
	public static void main(String[] args) {
		
		// test longs
		long start;
		long stop;
		
		File file = new File("data.txt");
		BufferedReader br = null;
		String line = null;
		ArrayTrial at = new ArrayTrial();
		
		try {
			
			br = new BufferedReader(new FileReader(file));
			
			line = br.readLine();
			
			while ((line = br.readLine()) != null) {
				at.insertData(line);
			}
			
			
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// Test our datastructure and data set
		String newData = "1928412,Mona,Lisa,ml@ml.no,female,10.0.0.1";
		start = at.startTime();
		boolean result = at.insertData(newData);
		stop = at.stopTime();
		at.results(start, stop);
		
		
		
	}

}
