package no.uib.info233.v2016.bigo.browser.trial;

public interface Trial {
	
	String findData(String term);
	
	boolean insertData(String data);
	
	boolean editData(String original, String replacement);
	
	default long startTime() {
		return System.nanoTime();
	}
	
	default long stopTime() {
		return System.nanoTime();
	}
	
	default void results(long start, long stop) {
		System.out.println("Operation took " + (stop - start) + " nanoseconds." );
	}
	

}
