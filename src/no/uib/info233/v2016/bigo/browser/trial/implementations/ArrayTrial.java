package no.uib.info233.v2016.bigo.browser.trial.implementations;

import java.util.ArrayList;

import no.uib.info233.v2016.bigo.browser.trial.Trial;

public class ArrayTrial implements Trial {
	
	private ArrayList<String> data;
	
	public ArrayTrial() {
		this.data = new ArrayList<>();
	}
	
	

	@Override
	public String findData(String term) {
		for (String e : data ) {
			if (e.contains(term)) return e;
		}
		return null;
	}

	@Override
	public boolean insertData(String data) {
		
		return this.data.add(data);
		
	}

	@Override
	public boolean editData(String original, String replacement) {
		
		if (data.contains(original)) {
			int i = data.indexOf(original);
			data.set(i, replacement);
			return true;
		}
		
		return false;
	}

}
